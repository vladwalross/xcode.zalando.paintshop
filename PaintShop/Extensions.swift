//
//  Extensions.swift
//  PaintShop
//
//  Created by Vladimir Urbano on 24-05-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import Foundation

extension Bool {
	var stringValue: String {
		if self {
			return "1"
		}
		
		return "0"
	}
}

extension String {
	var boolValue: Bool {
		return self == "1"
	}
	
	var intValue: Int? {
		if let i = Int(self) {
			return i
		}
		
		return nil
	}
}