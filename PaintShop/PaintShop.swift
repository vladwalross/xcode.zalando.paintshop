//
//  PaintShop.swift
//  PaintShop
//
//  Created by Vladimir Urbano on 23-05-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import Foundation

public class PaintShop {
	public init() {
		
	}
	
	public func makeBatches() -> [String] {
		var output = [String]()
		do {
			let input = try self.decodeInput()
			for testCase in 0 ..< input.cases.count {
				var mattes = [Int](count: input.cases[testCase].customers.count, repeatedValue: -1)
				var possibles = [Int](count: input.cases[testCase].customers.count, repeatedValue: 0)
				var matrix = [[Bool]](count: input.cases[testCase].customers.count, repeatedValue: [Bool](count: input.cases[testCase].paintColors, repeatedValue: false))
				for customer in 0 ..< input.cases[testCase].customers.count {
					for color in 0 ..< input.cases[testCase].customers[customer].likes.count {
						if input.cases[testCase].customers[customer].likes[color].colorIsMatte {
							mattes[customer] = input.cases[testCase].customers[customer].likes[color].paintColor
						} else {
							matrix[customer][input.cases[testCase].customers[customer].likes[color].paintColor] = true
							possibles[customer]++
						}
					}
				}
				var choosen = [Bool](count: input.cases[testCase].paintColors, repeatedValue: false)
				var used = [Bool](count: input.cases[testCase].customers.count, repeatedValue: false)
				var success = true
				var visited = true
				repeat {
					visited = false
					for j in 0 ..< input.cases[testCase].customers.count {
						if possibles[j] == 0 && !used[j] {
							used[j] = true
							visited = true
							if (mattes[j] == -1) {
								success = false
								break
							} else {
								choosen[mattes[j]] = true
								for k in 0 ..< input.cases[testCase].customers.count {
									if matrix[k][mattes[j]] {
										possibles[k]--
										matrix[k][mattes[j]] = false
									}
								}
							}
						}
					}
				} while (visited && success)
				
				if (success) {
					var a = "Case #\(testCase + 1):"
					for item in choosen {
						a += " \(item.stringValue)"
					}
					output.append(a)
				} else {
					output.append("Case #\(testCase + 1): IMPOSSIBLE")
				}
			}
		} catch {
			print("Error")
		}
		return output
	}
	
	public func decodeInput() throws -> Input {
		let entity = Input()
		if let filepath = self.getInputPath() {
			var testCases: Int?
			var colors: Int
			var customers: Int
			
			if let reader = StreamReader(path: filepath) {
				defer {
					reader.close()
				}
				
				var _next: String? = reader.nextLine()
				if let _cases = _next?.intValue {
					testCases = _cases
				} else {
					throw ParseError.NoCasesDeclared
				}
				
				if let _cases = testCases {
					for i in 1 ... _cases {
						_next = reader.nextLine()
						
						if let _colors = _next?.intValue {
							colors = _colors
						} else {
							throw ParseError.NoColorsDeclared
						}
						
						_next = reader.nextLine()
						
						if let _customers = _next?.intValue {
							customers = _customers
						} else {
							throw ParseError.NoCustomersDeclared
						}
						
						var testCase = TestCase(paintColors: colors)
						
						for j in 1 ... customers {
							var customer = Customer()
							if let _next = reader.nextLine() {
								let likes = _next.characters.split(" ").map(String.init)
								if let _numberLikes = likes[0].intValue {
									var index = 1
									for k in 1 ... _numberLikes {
										if let _color = likes[index].intValue {
											customer.likes.append(CustomerLike(paintColor: _color, colorIsMatte: likes[index + 1].boolValue))
										} else {
											throw ParseError.NoLikesDeclared
										}
										
										index += 2
									}
								}
							} else {
								throw ParseError.NoLikesDeclared
							}
							
							testCase.customers.append(customer)
						}
						
						entity.cases.append(testCase)
					}
				} else {
					throw ParseError.NoCasesDeclared
				}
			}
		}
		return entity
	}
	
	public func encodeInput(entity: Input) -> Bool {
		var s = "\(entity.cases.count)\n"
		for testCase in entity.cases {
			s += "\(testCase.paintColors)\n"
			s += "\(testCase.customers.count)\n"
			for customer in testCase.customers {
				s += "\(customer.likes.count)"
				for likes in customer.likes {
					s += " \(likes.paintColor) \(likes.colorIsMatte.stringValue)"
				}
				s += "\n"
			}
		}
		
		if let filepath = self.getInputPath() {
			do {
				let fileurl = NSURL(fileURLWithPath: filepath)
				try s.writeToURL(fileurl, atomically: true, encoding: NSUTF8StringEncoding)
				return true
			}
			catch {
			}
		}
		
		return false
	}
	
	public func setInput(input: String) {
		let man = NSFileManager.defaultManager()
		if let toPath = self.getInputPath() {
			let srcUrl = NSURL(fileURLWithPath: input)
			let toUrl = NSURL(fileURLWithPath: toPath)
			do {
				if man.fileExistsAtPath(toPath) {
					try man.removeItemAtURL(toUrl)
				}
				try man.copyItemAtURL(srcUrl, toURL: toUrl)
			} catch {
				print("Error")
			}
		}
	}
	
	public func getInputPath() -> String? {
		return self.getPath("input.in")
	}
	
	private func getOutputPath() -> String? {
		return self.getPath("output.out")
	}
	
	private func getPath(filename: String) -> String? {
		if let dir = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).last {
			let fileurl =  dir.URLByAppendingPathComponent(filename)
			return fileurl.absoluteString.stringByReplacingOccurrencesOfString("file://", withString: "")
		}
		
		return nil
	}
}
