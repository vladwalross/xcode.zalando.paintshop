//
//  Exceptions.swift
//  PaintShop
//
//  Created by Vladimir Urbano on 24-05-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import Foundation

enum ParseError : ErrorType {
	case NoCasesDeclared
	case NoColorsDeclared
	case NoCustomersDeclared
	case NoLikesDeclared
}