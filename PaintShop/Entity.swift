//
//  Entity.swift
//  PaintShop
//
//  Created by Vladimir Urbano on 23-05-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import Foundation

public class Input {
	// MARK: - Properties
	public var cases: [TestCase]
	
	// MARK: - Initializers
	public init() {
		self.cases = [TestCase]()
	}
}

public class TestCase {
	// MARK: - Properties
	public var paintColors: Int
	public var customers: [Customer]
	
	// MARK: - Initializers
	public init(paintColors: Int) {
		self.paintColors = paintColors
		self.customers = [Customer]()
	}
}

public class Customer {
	// MARK: - Properties
	public var likes: [CustomerLike]
	
	// MARK: - Initializers
	public init() {
		self.likes = [CustomerLike]()
	}
}

public class CustomerLike {
	// MARK: - Properties
	public var paintColor: Int
	public var colorIsMatte: Bool
	
	// MARK: - Initializers
	public init(paintColor: Int, colorIsMatte: Bool) {
		self.paintColor = paintColor
		self.colorIsMatte = colorIsMatte
	}
}
