//
//  PaintShopTests.swift
//  PaintShopTests
//
//  Created by Vladimir Urbano on 23-05-16.
//  Copyright © 2016 Vladimir Urbano. All rights reserved.
//

import XCTest
@testable import PaintShop

class PaintShopTests: XCTestCase {
	var paintShop: PaintShop!
    
    override func setUp() {
        super.setUp()
		
		self.paintShop = PaintShop()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
	}
	
	func testSimpleDataset() {
		self.paintShop.setInput("/Volumes/Rauenstein/Users/vla/Xcode/Zalando/simple.html")
		self.paintShop.makeBatches()
	}
	
	func testSmallDataset() {
		self.paintShop.setInput("/Volumes/Rauenstein/Users/vla/Xcode/Zalando/small.html")
		self.paintShop.makeBatches()
	}
	
	func testLargeDataset() {
		self.paintShop.setInput("/Volumes/Rauenstein/Users/vla/Xcode/Zalando/large.html")
		self.paintShop.makeBatches()
	}
}
